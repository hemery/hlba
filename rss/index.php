<head>
<title>Actualité</title>
<link rel="stylesheet" type="text/css" href="index.css" media="all"/>
<script type="text/javascript" src="index.js"></script>
<meta charset="UTF-8"/>
</head>
<body>
<header>
<h1>Actualité</h1>
</header>
<h2>Jeux vidéo</h2>
<?php
require_once("feedparser.php");
echo FeedParser("http://www.gameblog.fr/rss.php");
?>
<h2>Hardware</h2>
<h3>Le comptoir du hardware</h3>
<?php
require_once("feedparser.php");
echo FeedParser("http://www.comptoir-hardware.com/actus.xml");
?>
<h3>Ginjfo</h3>
<?php
require_once("feedparser.php");
echo FeedParser("http://www.ginjfo.com/actualites/feed");
?>
<h3>Hardware.fr</h3>
<?php
require_once("feedparser.php");
echo FeedParser("http://feeds.feedburner.com/hardware/fr/news");
?>
<h2>Software</h2>
<h3>Developpez.com</h3>
<?php
require_once("feedparser.php");
echo FeedParser("http://www.developpez.com/index/rss");
?>
</section>
</body>
